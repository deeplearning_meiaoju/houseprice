##
## auto-dr: dimensionality reduction
##

library(dplyr)
library(randomForest)

yield <- function (years, fmt) {
    acs5 <- lapply(years, function (year) {
        read.csv(sprintf(fmt, year), header = TRUE)
    })
    names(acs5) <- paste("y", years, sep = "")

    ## filter out house value not available
    acs5 <- lapply(acs5, function (df) {
        filter(df, !(is.na(B25076_001E) | is.na(B25077_001E) | is.na(B25078_001E)))
    })

    ## filter out not available columns
    cols.na <- unique(do.call(c, lapply(acs5, function (data) {
        names(data)[apply(data, 2, function (v) any(is.na(v)))]
    })))
    acs5 <- lapply(acs5, function (df) {
        select(df, -one_of(cols.na))
    })

    ## .y{year}
    for (k in names(acs5)) {
        variables <- names(acs5[[k]])
        names(acs5[[k]]) <- ifelse(variables %in% c("state", "county", "tract", "block.group"),
                                   variables,
                                   paste(variables, k, sep = "."))
    }

    ## join on (state, county, tract)
    joint = NULL
    for (df in acs5) {
        if (is.null(joint)) {
            joint <- df
            next
        }
        joint <- merge(joint, df)
    }

    joint
}

resolve <- function (acs5, response_year, ntest = 100) {
    X <- select(acs5, matches(sprintf(".*_.*\\.y(%d|%d)$", response_year - 3, response_year - 2)))
    Y <- 100 * (acs5$B25077_001E.y2013 / acs5$B25077_001E.y2011 - 1)

    indices.training <- sample(1:length(Y), length(Y) - ntest)
    list(
        training = list(
            X = X[indices.training,],
            Y = Y[indices.training]),
        testing = list(
            X = X[-indices.training,],
            Y = Y[-indices.training]))
}

## ------------------------------------------------------------
## prRandomForest: randomForest, with prcomp

prRandomForest <- function (x, y, ncomp) {
    pr.fit <- prcomp(x)
    x <- data.frame(predict(pr.fit, newdata = x))

    rf.fit <- randomForest(y ~., data = x[1:ncomp])

    structure(
        list(pr = pr.fit, rf = rf.fit, ncomp = ncomp),
        class = "prRandomForest")
}

predict.prRandomForest <- function (fit, x) {
    x <- data.frame(predict(fit$pr, newdata = x))
    predict(fit$rf, x[1:fit$ncomp])
}

## ------------------------------------------------------------
## sisRandomForest
library(SIS)

sisRandomForest <- function (x, y) {
    si <- sample(1:nrow(x), min(nrow(x), ncol(x) / 3))
    sis.fit <- SIS(as.matrix(x[si,]), y[si])
    rf.fit <- randomForest(y ~., data = x[sis.fit$ix])

    structure(
        list(sis = sis.fit, rf = rf.fit),
        class = "sisRandomForest")
}

predict.sisRandomForest <- function (fit, x) {
    predict(fit$rf, newdata = x[fit$sis$ix])
}

## ------------------------------------------------------------
## fuzzy check

fuzzy_check <- function (y, p, gr.size = 5) {
    bench <- data.frame(y, p)
    bench$p_rank <- rank(desc(p), ties.method = "random")
    bench$p_group <- bench$p_rank %/% gr.size

    bench %>%
        group_by(p_group) %>%
        summarise(actual_avg = mean(y), pred_avg = mean(p))
}

fuzzy_plot <- function (y, p, gr.size = 5) {
    barplot(t(as.matrix(fuzzy_check(y, p, gr.size)[c("actual_avg", "pred_avg")])),
            col = c("#3762b5", "#aec4dc"), beside = TRUE)
}

## ------------------------------------------------------------
## testdrive

acs5 <- yield(2010:2014, "all_tracts_santa_clara_%d.csv") %>% resolve(2013)

## prcomp
prrf.fit <- prRandomForest(acs5$training$X, acs5$training$Y, 27)
Y.prrf <- predict(prrf.fit, acs5$testing$X)
cor(acs5$testing$Y, Y.prrf)
fuzzy_plot(acs5$testing$Y, Y.prrf)

## SIS
sisrf.fit <- sisRandomForest(acs5$training$X, acs5$training$Y)
Y.sisrf <- predict(sisrf.fit, acs5$testing$X)
cor(acs5$testing$Y, Y.sisrf)
fuzzy_plot(acs5$testing$Y, Y.sisrf)
