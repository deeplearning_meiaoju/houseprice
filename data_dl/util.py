import json
import requests
import pandas as pd
from pandas import Series, DataFrame

def get_varibles(type):

    variables_df = pd.read_csv("acs dataset names - tables_used.csv")
    if type=="acs":
        return [variables_df[variables_df["type"]=="acs"]["Variable_name"].tolist(),
                variables_df[variables_df["type"]=="acs"]["new_name"].tolist()]
    else:
        return [variables_df[variables_df["type"]!="acs"]["Variable_name"].tolist(),
                variables_df[variables_df["type"]!="acs"]["new_name"].tolist()]


def acs_fetch_one_by_year(varialbes_name, county, state, year, key):
    tract_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
    if type(varialbes_name) is str:
        get_clause = "get="+varialbes_name
    else:
    # it is a list of strings
        get_clause = "get="+(",".join(varialbes_name))
    tract_url += get_clause
    tract_url += "&for=tract:*&in=state:" + state
    tract_url += "+county:" + county
    tract_url += "&key=" + key
    r = json.loads(requests.get(tract_url).text)
    header = r.pop(0)
    result=DataFrame(r, columns=header)
    return result

def acs_fetch_all_tract_years(acs_variables, county, state, years, key):

    df = DataFrame({'state':state, 'county':county}, index=[0])
    for year in years:
        single_year_df = acs_fetch_one_by_year(acs_variables[0], county, state, year,key)
        single_year_df.dropna(axis=0, how='any',thresh=None, subset=acs_variables[0], inplace=True)
        single_year_df[acs_variables[0]]=single_year_df[acs_variables[0]].applymap(lambda x:float(x))

        # now replace all variable names with their more meaningful counterparts
        single_year_df.rename(columns={x:y for (x,y) in zip(acs_variables[0],acs_variables[1])},inplace=True)
        single_year_df = single_year_df.query('population_25>0 and total_population>0')
        # calculate bachelor degree holder percentage in population 25+
        single_year_df["B_degree_pc"] = single_year_df.apply(
          lambda row: (row["male_bachelors"]+row["female_bachelors"])/row["population_25"],axis=1)
        single_year_df.drop(["male_bachelors","female_bachelors","population_25"],axis=1,inplace=True)
        single_year_df["hispanic_pc"] = single_year_df.apply(
          lambda row: row["hispanics"]*1.0/row["total_population"],axis=1)
        single_year_df["black_pc"] = single_year_df.apply(
          lambda row: row["blacks"]*1.0/row["total_population"],axis=1)
        single_year_df.drop(["hispanics","blacks"],axis=1,inplace=True)
        # get the variable list
        variable_list = single_year_df.columns-["state","county","tract"]
        single_year_df.rename(
          columns={variable:(variable+"_"+str(year)) for variable in variable_list},inplace=True)
        print len(single_year_df)
        #merge with existing df
        # if 'tract' is not one of df's columns, it means this is the first merge (df is empty)
        # use only state and county
        if 'tract' not in df.columns:
          df = pd.merge(df, single_year_df, on=['state','county'])
          # rearrange columns so state, county, tract are the first three
          df = df[['state','county','tract']+(df.columns.difference(['state','county','tract'])).tolist()]
        else:
          # this is not the first merge, use state, county and tract columns to merge
          df = pd.merge(df, single_year_df, on=['state','county','tract'])
    return df

def census_fetch_all_tract_2000(variable_names, county, state, key):
  '''
  Method to get 2010 census data for all tracts in a county
  Input:
    variable_names: list of names of variables to be fetched (in the coded, i.e., non-readable` format)
    county: county code
    state: state code
    year: four-digit year
    key: API key
  Output:
    a dict that has the variable_names as key and their value as item
  '''
  tract_url = "http://api.census.gov/data/2000/sf3?"
  get_clause = "get=" +(",".join(variable_names))
  tract_url += get_clause
  tract_url += "&for=tract:*&in=state:" + state
  tract_url += "+county:" + county
  tract_url += "&key=" + key
  r = json.loads(requests.get(tract_url).text)
  header = r.pop(0)
  result=DataFrame(r, columns=header)
  return result

def census_data_processing_2000(census_variables, county, state, key):
  '''
  This method calls census_fetch_all_tract_2000() and process the data
  Input:
  census_variables:  list of two lists: variable names (for data downloading)
                     and new more meaningful names variables
  state: state code
  county: county code
  Output: DataFrame of census data

  '''
  variables_to_fetch = census_variables[0]
  census_df = census_fetch_all_tract_2000(variables_to_fetch,county=county,state=state, key=key)
  census_df.dropna(axis=0, how='any', thresh=None, subset=variables_to_fetch, inplace=True)
  census_df[variables_to_fetch]=census_df[variables_to_fetch].applymap(lambda x:float(x))
  census_df.rename(columns={x:y for (x,y) in zip(variables_to_fetch,census_variables[1])},inplace=True)
  # keep only rows with positive population
  census_df = census_df.query('population_25>0 and total_population>0')
  try:
    census_df["B_degree_pc"] = census_df.apply(
    lambda row: (row["male_bachelors"]+row["male_masters"]+row["male_professionals"]+row["male_doctorals"] +
                     row["female_bachelors"]+row["female_masters"]+row["female_professionals"]+row["female_doctorals"])/row["population_25"] if row["population_25"]>0 else 0,axis=1)
  except ZeroDivisionError:
    census_df.to_csv("test.csv")

  census_df.drop(["male_bachelors","male_masters","male_professionals","male_doctorals",
                  "female_bachelors","female_masters","female_professionals","female_doctorals"],axis=1,inplace=True)
  census_df["hispanic_pc"]=census_df.apply(lambda row: row["hispanics"]*1.0/row["total_population"],axis=1)
  census_df["black_pc"]=census_df.apply(lambda row: row["blacks"]*1.0/row["total_population"],axis=1)
  census_df.drop(["hispanics","blacks"],axis=1,inplace=True)
  variable_list = census_df.columns.difference(['state','county','tract'])
  census_df.rename(columns={variable:(variable+"_2000") for variable in variable_list},inplace=True)
  # Move state, county and tract to make them the first three columns of the df
  census_df = census_df[['state','county','tract']+(census_df.columns.difference(['state','county','tract'])).tolist()]
  return census_df
