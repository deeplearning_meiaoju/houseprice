import sys,json,requests
from pandas import DataFrame, Series
import pandas as pd
import re
from time import sleep
my_key="099998b0adf21394aa1dd2c62bae345cc5cc3645"
# replace state and county values as appropriate
# state, county="12","103"
state, county = "06","037"
# 12 and 103 are for Florida and Pinellas County, respectively

def acs_fetch_block_groups(variable_names, tract, county, state, year, key=my_key):
  '''
  Method to get ACS data for all black groups in a given tract in a tract in a county
  Input:
  variable_names: one variable name (string) or a list of names of variables to be fetched (still requires a list even for one variable)
  tract: tract number
  county: county code
  state: state code
  year: four-digit year
  key: API key 
  Output:
  None if there is error retrieving the value, otherwise a DataFrame with one row for each block group
  '''
  block_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
  # construct the get clause
  if type(variable_names)==list:
    # if it's a list, use "," to join the elements
    get_clause = "get="+(",".join(variable_names))
  else:
    # it's just one string
    get_clause = "get="+variable_names
  block_url += get_clause
  block_url += "&for=block+group:*&in=state:" + state
  new_tract = prepare_tract_value(tract)
  block_url += "+county:" + county + "+tract:" + new_tract
  block_url += "&key=" + key
  #print "get_clause=", get_clause
  print block_url
  r0 = requests.get(block_url)
  #print r0, r0.text
  if r0.text.startswith("error"):
    return None
  r = json.loads(r0.text)
  header = r.pop(0)
  result = DataFrame(r, columns=header)
  return result

def prepare_tract_value(tract):
  '''
  This method converts a regular-format tract number to a five-character strings
  For example: 
  For input: 201.01, output is 20101
  For input: 204, output is 20400
  '''
  new_tract = tract.replace('.','')
  if len(new_tract)<5:
    new_tract = new_tract + '0'*(5-len(new_tract))
  return new_tract

def main():

  # census_raw=pd.read_csv("DEC_10_SF1_H1.csv")
  census_raw=pd.read_csv("LA_DEC_10_SF1_H1.csv")
  # census tract is in the GEO.display-label column
  tracts = census_raw["GEO.display-label"].apply(lambda x:re.search(r'Census Tract (.*?)[,\s]',x).group(1))
  # drop the last tract, as it is 9901 and not valid
  tracts.drop(len(tracts)-1,inplace=True)
  with open("acs5_variables.json") as json_file:
    json_data = json.load(json_file)
  # only variables with E ending is estimate of a variable (those ending with M are 
  # margin of error)
  vars = [name for name in json_data['variables'].keys() if name[-1]=='E']
  year = 2010
  # result for the first variable for the first tract
  result = acs_fetch_block_groups(vars[0],tracts[0],county,state,year)
  # concatenate rows for each tract
  for tract in tracts[1:]:
    result_new = acs_fetch_block_groups(vars[0],tract,county,state,year)
    result = result.append(result_new)
  # drop the first column since we are only interested in block groups
  result.drop(vars[0],axis=1,inplace=True)
  result.to_csv("block_groups_LA.csv",index=False)

if __name__ == '__main__':
  main()