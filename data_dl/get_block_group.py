import argparse,json,sys,requests

from pandas import DataFrame, Series
import pandas as pd
import re

def get_block_group(state, county, year):
    api_block = 'http://api.census.gov/data/'+year+'/acs5?get=NAME,B01001_001E&for=block+group:*&in=state:'+state+'+county:'+county
    print api_block
    r0 = requests.get(api_block)
    if r0.text.startswith("error"):
        return None
    r = json.loads(r0.text)
    header = r.pop(0)
    result = DataFrame(r, columns=header)
    result.drop(["NAME", "B01001_001E"],axis=1,inplace=True)
    result.to_csv(state+county+year+".csv",index=False)
    return result

def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', nargs="?", help="state")
    parser.add_argument('-c', nargs="?", help="county")
    parser.add_argument('-y', nargs="?", help="year")
    args = parser.parse_args()
    get_block_group(args.s, args.c, args.y)


if __name__ == '__main__':
    run()