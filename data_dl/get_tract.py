import argparse,sys,json,requests

import pandas as pd
from pandas import Series, DataFrame

import util



def run():

    frames = []

    area = [{'county':'081', "state": "06"},{'county':'085', "state": "06"},{'county':'001', "state": "06"}]
    for a in area:
        acs_variables = util.get_varibles('acs')
        years = range(2010, 2015)
        acs_df = util.acs_fetch_all_tract_years(acs_variables, a['county'], a['state'], years, '099998b0adf21394aa1dd2c62bae345cc5cc3645')
        census_variables = util.get_varibles("census")
        census_df = util.census_data_processing_2000(census_variables, a['county'], a['state'], '099998b0adf21394aa1dd2c62bae345cc5cc3645')
        common_df=pd.merge(census_df,acs_df,on=["tract","county","state"])
        frames.append(common_df)

    result = pd.concat(frames)
    common_df.to_csv("Silicon_Valley.csv",index=False)

if __name__ == "__main__":
    run()