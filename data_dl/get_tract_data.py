import sys
import json
import requests
import pandas as pd
from pandas import Series, DataFrame

county, state= "103", "12"
# API key
my_key="099998b0adf21394aa1dd2c62bae345cc5cc3645"

def get_variables(type):
  '''
  Get variable names and their meaning from a csv file, then put them in a list of two lists
  Input:
  type: either "census" or "acs"
  Output:
  A list of two lists, the first one being the actual variable names in the data,
  the second list more meaningful names correspondingly.
  For example, the return can be 
  [["B19013_001E","B25077_001E",	"B21003_001E"],
  ["income","house_value","population_25"]]
  '''
  variables_df = pd.read_csv("acs dataset names - tables_used.csv")
  if type=="acs":
    return [variables_df[variables_df["type"]=="acs"]["Variable_name"].tolist(),
            variables_df[variables_df["type"]=="acs"]["new_name"].tolist()]
  else:
    # otherwise it's 2000 census
    return [variables_df[variables_df["type"]!="acs"]["Variable_name"].tolist(),
            variables_df[variables_df["type"]!="acs"]["new_name"].tolist()]

def acs5_fetch_single_tract(variable_names,tract, county, state, year,key=my_key):
  '''
  Method to fetch ACS5 data through the API for a single tract
  Input:
  variable_names: a list of names of variables to be fetched
  tract: tract number
  county: county code
  state: state code
  year: four-digit year
  key: API key 
  Output:
  a dict that has the variable_names as key and their value as item
  '''
  tract_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
  # construct the get clause
  get_clause = "get="+(",".join(variable_names))
  tract_url += get_clause
  tract_url += "&for=tract:" + str(tract)
  tract_url += "&in=state:" + state
  tract_url += "+county:" + county
  tract_url += "&key=" + key
  response=requests.get(tract_url)
  r = json.loads(response.text)
  header = r.pop(0)
  result=DataFrame(r, columns=header)
  return result

def census_fetch_all_tract_2000(variable_names, county, state, key=my_key):
  '''
  Method to get 2010 census data for all tracts in a county
  Input:
    variable_names: list of names of variables to be fetched (in the coded, i.e., non-readable` format)
    county: county code
    state: state code
    year: four-digit year
    key: API key 
  Output:
    a dict that has the variable_names as key and their value as item
  '''
  tract_url = "http://api.census.gov/data/2000/sf3?"
  get_clause = "get=" +(",".join(variable_names))
  tract_url += get_clause
  tract_url += "&for=tract:*&in=state:" + state
  tract_url += "+county:" + county
  tract_url += "&key=" + key
  r = json.loads(requests.get(tract_url).text)
  header = r.pop(0)
  result=DataFrame(r, columns=header)
  return result

def acs_fetch_all_tracts(variable_names, county, state, year, key=my_key):
  '''
  Method to get ACS data for all tracts in a county
  Input:
  variable_names: list of names of variables to be fetched, or if just one name, just the str 
  county: county code
  state: state code
  year: four-digit year
  key: API key 
  Output:
  a dict that has the variable_names as key and their value as item
  '''
    
  tract_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
  # construct the get clause
  if type(variable_names) is str:
    get_clause = "get="+variable_names
  else:
    # it is a list of strings
    get_clause = "get="+(",".join(variable_names))
  tract_url += get_clause
  tract_url += "&for=tract:*&in=state:" + state
  tract_url += "+county:" + county
  tract_url += "&key=" + key
  print tract_url
  r = json.loads(requests.get(tract_url).text)
  header = r.pop(0)
  result=DataFrame(r, columns=header)
  return result 

def acs_fetch_all_tract_all_years(acs_variables, county, state, years, key=my_key):
  ''' 
  Method to get variables for all tracts for all years in a county
  Input:
  acs_variables: list of two lists, first list is variable names, second list is their corresponding meaning
  years: list of years to fetch
  county: county code
  state: state code
  key: API key 
  Output:
    DataFrame with the state, county and tract as the first three columns, and rest variables
    labeled by _year, e.g., xxxx_2010
  '''
  df = DataFrame({'state':state,'county':county},index=[0])
  for year in years:
    print "year is "+str(year)
    single_year_df=acs_fetch_all_tracts(acs_variables[0],county=county,state=state,year=year)
    single_year_df.dropna(axis=0, how='any',thresh=None, subset=acs_variables[0], inplace=True)
    # convert those variable columns to float
    single_year_df[acs_variables[0]]=single_year_df[acs_variables[0]].applymap(lambda x:float(x))
    # now replace all variable names with their more meaningful counterparts
    single_year_df.rename(columns={x:y for (x,y) in zip(acs_variables[0],acs_variables[1])},inplace=True)
    single_year_df = single_year_df.query('population_25>0 and total_population>0')
    # calculate bachelor degree holder percentage in population 25+
    single_year_df["B_degree_pc"] = single_year_df.apply(
      lambda row: (row["male_bachelors"]+row["female_bachelors"])/row["population_25"],axis=1)
    single_year_df.drop(["male_bachelors","female_bachelors","population_25"],axis=1,inplace=True)
    single_year_df["hispanic_pc"] = single_year_df.apply(
      lambda row: row["hispanics"]*1.0/row["total_population"],axis=1)
    single_year_df["black_pc"] = single_year_df.apply(
      lambda row: row["blacks"]*1.0/row["total_population"],axis=1)
    single_year_df.drop(["hispanics","blacks"],axis=1,inplace=True)
    # get the variable list 
    variable_list = single_year_df.columns-["state","county","tract"]
    single_year_df.rename(
      columns={variable:(variable+"_"+str(year)) for variable in variable_list},inplace=True)
    print len(single_year_df)
    #merge with existing df
    # if 'tract' is not one of df's columns, it means this is the first merge (df is empty)
    # use only state and county 
    if 'tract' not in df.columns:
      df = pd.merge(df, single_year_df, on=['state','county'])
      # rearrange columns so state, county, tract are the first three
      df = df[['state','county','tract']+(df.columns.difference(['state','county','tract'])).tolist()]
    else:
      # this is not the first merge, use state, county and tract columns to merge
      df = pd.merge(df, single_year_df, on=['state','county','tract'])
  return df

def census_data_processing_2000(census_variables, county, state):
  '''
  This method calls census_fetch_all_tract_2000() and process the data
  Input:
  census_variables:  list of two lists: variable names (for data downloading) 
                     and new more meaningful names variables 
  state: state code
  county: county code
  Output: DataFrame of census data
  
  '''
  variables_to_fetch = census_variables[0]
  census_df = census_fetch_all_tract_2000(variables_to_fetch,county=county,state=state)
  census_df.dropna(axis=0, how='any', thresh=None, subset=variables_to_fetch, inplace=True)
  census_df[variables_to_fetch]=census_df[variables_to_fetch].applymap(lambda x:float(x))
  census_df.rename(columns={x:y for (x,y) in zip(variables_to_fetch,census_variables[1])},inplace=True)
  # keep only rows with positive population
  census_df = census_df.query('population_25>0 and total_population>0')
  try:
    census_df["B_degree_pc"] = census_df.apply(
    lambda row: (row["male_bachelors"]+row["male_masters"]+row["male_professionals"]+row["male_doctorals"] +
                     row["female_bachelors"]+row["female_masters"]+row["female_professionals"]+row["female_doctorals"])/row["population_25"] if row["population_25"]>0 else 0,axis=1)
  except ZeroDivisionError:
    print year
    census_df.to_csv("test.csv")
    
  census_df.drop(["male_bachelors","male_masters","male_professionals","male_doctorals",
                  "female_bachelors","female_masters","female_professionals","female_doctorals"],axis=1,inplace=True)
  census_df["hispanic_pc"]=census_df.apply(lambda row: row["hispanics"]*1.0/row["total_population"],axis=1)
  census_df["black_pc"]=census_df.apply(lambda row: row["blacks"]*1.0/row["total_population"],axis=1)
  census_df.drop(["hispanics","blacks"],axis=1,inplace=True)
  variable_list = census_df.columns.difference(['state','county','tract'])
  census_df.rename(columns={variable:(variable+"_2000") for variable in variable_list},inplace=True)
  # Move state, county and tract to make them the first three columns of the df
  census_df = census_df[['state','county','tract']+(census_df.columns.difference(['state','county','tract'])).tolist()]
  return census_df


def main():
  acs_variables = get_variables("acs")
  print acs_variables
  # acs_variables is a list of two lists: variable names and new more meaningful names
  # get 2010 acs data
  years = range(2010,2015)
  acs_df = acs_fetch_all_tract_all_years(acs_variables, county, state, years)
 
  # now get 2000 census data
  census_variables = get_variables("census")
  census_df = census_data_processing_2000(census_variables, county, state)
  
  common_df=pd.merge(census_df,acs_df,on=["tract","county","state"])
  common_df.to_csv("FL_common_tracts_new.csv",index=False)  
  # First, get the counties in the bottom 40% quantile in both median household income and median house value in 2009
  #poor_tracts = sc_df[(sc_df["B19013_001E_2010"]<=sc_df["B19013_001E_2010"].quantile(0.4)) & 
  #                      (sc_df["B25077_001E_2010"]<=sc_df["B25077_001E_2010"].quantile(0.4))]
  # calculate increase in percentage for median house price and bachelor percentage
  #sc_df["housing_increase"] = sc_df.apply(lambda x: (x['B25077_001E_2013']-x['B25077_001E_2010'])/x['B25077_001E_2010']
  #                                       ,axis=1)
  #sc_df["B_increase"] = sc_df.apply(lambda x:(x['B_degree_pc_2013']-x['B_degree_pc_2010'])/x['B_degree_pc_2010']
  #                                  ,axis=1)
  #sc_df["region"] = sc_df.apply(lambda x: x['state']+x['county']+x['tract'],axis=1)
  # Find those rows that have both the above two increases (housing price and bachelor degree holder) in the top 3% percentile
  #top_increased_tracts = sc_df[(sc_df["housing_increase"]>=sc_df["housing_increase"].quantile(0.7)) & 
  #                    (sc_df["B_increase"]>=sc_df["B_increase"].quantile(0.7))]
  #gentrified_tracts=pd.merge(poor_tracts,top_increased_tracts)
  
if __name__ == '__main__':
  main()
