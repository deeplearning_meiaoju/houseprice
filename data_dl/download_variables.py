import sys,json,requests
from pandas import DataFrame, Series
import pandas as pd
import re
from time import sleep
import numpy as np

my_key="099998b0adf21394aa1dd2c62bae345cc5cc3645"
# replace state and county values as appropriate
state, county="12","103"
# 12 and 103 are for Florida and Pinellas County, respectively

def acs_fetch_block_groups(variable_names, tract, county, state, year, key=my_key):
  '''
  Method to get ACS data for all black groups in a given tract in a tract in a county
  Input:
  variable_names: one variable name (string) or a list of names of variables to be fetched (still requires a list even for one variable)
  tract: tract number
  county: county code
  state: state code
  year: four-digit year
  key: API key 
  Output:
  None if there is error retrieving the value, otherwise a DataFrame with one row for each block group
  '''
  block_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
  # construct the get clause
  if type(variable_names)==list:
    # if it's a list, use "," to join the elements
    get_clause = "get="+(",".join(variable_names))
  else:
    # it's just one string
    get_clause = "get="+variable_names
  block_url += get_clause
  block_url += "&for=block+group:*&in=state:" + state
  # if tract is already in the right format, just convert it to string
  # otherwise call a function to convert it to the right format
  if len(str(tract))==5:
    new_tract = str(tract)
  else:
    new_tract = prepare_tract_value(tract)
  block_url += "+county:" + county + "+tract:" + new_tract
  block_url += "&key=" + key
  #print "get_clause=", get_clause
  print block_url
  r0 = requests.get(block_url)
  #print r0, r0.text
  if r0.text.startswith("error"):
    return None
  r = json.loads(r0.text)
  header = r.pop(0)
  result = DataFrame(r, columns=header)
  return result

def acs_fetch_one_block_group(variable_names, tract, block_group, county, state, year, key=my_key):
  '''
  Method to get ACS data for one black group in a given tract in a tract in a county
  Input:
  variable_names: one variable name (string) or a list of names of variables to be fetched (still requires a list even for one variable)
  tract: tract number
  block_group: block group number
  county: county code
  state: state code
  year: four-digit year
  key: API key 
  Output:
  None if there is error retrieving the value, otherwise a DataFrame with one row of data
  '''
  block_url = "http://api.census.gov/data/" + str(year) + "/acs5?"
  # construct the get clause
  if type(variable_names)==list:
    # if it's a list, use "," to join the elements
    get_clause = "get="+(",".join(variable_names))
  else:
    # it's just one string
    get_clause = "get="+variable_names
  block_url += get_clause
  block_url += "&for=block+group:"+ block_group 
  block_url += "&in=state:" 
  block_url += str(state)
  # if tract is already in the right format, just convert it to string
  # otherwise call a function to convert it to the right format
  if len(str(tract))==5:
    new_tract = str(tract)
  else:
    new_tract = prepare_tract_value(tract)
  block_url += "+county:" + county + "+tract:" + new_tract
  block_url += "&key=" + key
  #print "get_clause=", get_clause
  print block_url
  r0 = requests.get(block_url)
  #print r0, r0.text
  if r0.text.startswith("error"):
    return None
  r = json.loads(r0.text)
  header = r.pop(0)
  result = DataFrame(r, columns=header)
  return result

def prepare_tract_value(tract):
  '''
  This method converts a regular-format tract number to a five-character strings
  For example: 
  For input: 201.01, output is 20101
  For input: 204, output is 20400
  '''
  new_tract = tract.replace('.','')
  if len(new_tract)<5:
    new_tract = new_tract + '0'*(5-len(new_tract))
  return new_tract

def main():
  non_null_variables = pd.read_csv("non_null_vars.csv",header=None)
  block_groups_data = pd.read_csv("block_groups_StPete.csv")
  block_groups_data=block_groups_data.applymap(str)
  # get the list of variables from the one-column data frame
  vars = non_null_variables.iloc[:,0].tolist()
  # create a list of variables to get (100 at a time)
  var_start = np.arange(0,len(vars),step=50)
  # var_start is [0,100,200,...,2900]
  # generate variables blocks, i.e. list of lists of up to 100 variables each
  var_blocks = np.array([[var_start[i],min(var_start[i]+50,len(vars))] for i in range(len(var_start))])
  # var_blocks is [[0,50],[50,100],...,[2850,2900],[2900,..,2936]]
  
  # create a Series that stores data for all the variables for the first block group
  bg_first = block_groups_data.iloc[0,:]
  result_single = bg_first
  # result_single now only has four elements: ['tract','block group','county','state']
  for var_index_start,var_index_end in var_blocks:
    # download variables for one tract (and up to 50) at a time
    print var_index_start, var_index_end
    result_single_new = acs_fetch_one_block_group(vars[var_index_start:var_index_end],bg_first['tract'],bg_first['block group'],bg_first['county'],bg_first['state'],'2010')
    # take the first (and only) row of the data frame
    result_single_new = result_single_new.iloc[0,:]
    # drop the four identification columns since they are already in result_single
    result_single_new.drop(['tract','block group','county','state'],inplace=True)
    result_single=result_single.append(result_single_new)
    # result_single is a Series
  # convert it to DataFrame
  result = result_single.to_frame().T
    
  # the following goes through each block and download all (2940) variables at a time, then append it to the data frame
  # it is very time consuming
  for i, bg in block_groups_data.iloc[1:,:].iterrows():
    result_single = bg.copy()
    for var_index_start,var_index_end in var_blocks:
        # download variables for one tract (and up to 100) at a time
        print var_index_start, var_index_end
        result_single_new = acs_fetch_one_block_group(vars[var_index_start:var_index_end],bg['tract'],bg['block group'],bg['county'],bg['state'],'2010')
        # take the first (and only) row of the data frame
        result_single_new = result_single_new.iloc[0,:]
        result_single_new.drop(['tract','block group','county','state'],inplace=True)
        result_single=result_single.append(result_single_new)
    result = result.append(result_single,ignore_index=True)
  result.to_csv("st_pete_blocks_2010.csv")
  
if __name__ == '__main__':
  main()

  